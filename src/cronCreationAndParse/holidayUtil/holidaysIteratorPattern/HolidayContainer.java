package cronCreationAndParse.holidayUtil.holidaysIteratorPattern;

/**
 * @author 李朔
 * @version 1.0
 */
public interface HolidayContainer {

    public HolidayIterator getHolidayIterator();
}
