package cronCreationAndParse.holidayUtil.holidaysIteratorPattern;

/**
 * @author 李朔
 * @version 1.0
 */
public interface HolidayIterator {

    public boolean hasNext();
    public Object next();
}
