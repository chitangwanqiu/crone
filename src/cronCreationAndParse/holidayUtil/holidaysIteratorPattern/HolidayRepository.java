package cronCreationAndParse.holidayUtil.holidaysIteratorPattern;



import cronCreationAndParse.holidayUtil.holiday.Holiday;
import cronCreationAndParse.holidayUtil.holidaysFilterPattern.Criteria;

import java.util.List;

/**
 * @author 李朔
 * @version 1.0
 */
public class HolidayRepository implements HolidayContainer {

    private List<Holiday> holidays;
    //构造器
    public HolidayRepository(Criteria criteria, List<Holiday> holidays){
                this.holidays=criteria.meetCriteria(holidays);
    }
    @Override
    public HolidayIterator getHolidayIterator(){
        return new HolidayNewIterator();
    }


    //内部类
    public class HolidayNewIterator implements HolidayIterator {

        int index;
        @Override
        public boolean hasNext() {
            if(index<holidays.size()){
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if(this.hasNext()){
                return holidays.get(index++);
            }
            return null;
        }
    }

}
