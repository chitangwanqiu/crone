package cronCreationAndParse.holidayUtil.holidaysFilterPattern;

import cronCreationAndParse.holidayUtil.holiday.Holiday;

import java.util.List;

/**
 * @author 李朔
 * @version 1.0
 */
public interface Criteria {

    public List<Holiday> meetCriteria(List<Holiday> holidays);
}
