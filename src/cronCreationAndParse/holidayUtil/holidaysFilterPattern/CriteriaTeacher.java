package cronCreationAndParse.holidayUtil.holidaysFilterPattern;
import cronCreationAndParse.holidayUtil.holiday.Holiday;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 李朔
 * @version 1.0
 */
public class CriteriaTeacher implements Criteria{
    @Override
    public List<Holiday> meetCriteria(List<Holiday> holidays){
         List<Holiday> teacherHolidays=new ArrayList<>();
         for(Holiday holiday:holidays){
             teacherHolidays.add(holiday);
         }
        return teacherHolidays;
    }
}
