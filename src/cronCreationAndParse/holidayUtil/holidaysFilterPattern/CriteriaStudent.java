package cronCreationAndParse.holidayUtil.holidaysFilterPattern;



import cronCreationAndParse.holidayUtil.holiday.Holiday;
import cronCreationAndParse.holidayUtil.holiday.TeacherFestival;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 李朔
 * @version 1.0
 */
public class CriteriaStudent implements Criteria{
    @Override
    public List<Holiday> meetCriteria(List<Holiday> holidays){
        List<Holiday> studentHolidays=new ArrayList<>();
        for(Holiday holiday:holidays){
            if(holiday.getClass().getName().equals(TeacherFestival.class.getName())){
                continue;
            }
            studentHolidays.add(holiday);
        }
        return studentHolidays;
    }
}
