package cronCreationAndParse.holidayUtil.holidaysFilterPattern;


import cronCreationAndParse.holidayUtil.holiday.Holiday;
import cronCreationAndParse.holidayUtil.holiday.SpringFestival;
import cronCreationAndParse.holidayUtil.holiday.TeacherFestival;
import cronCreationAndParse.holidayUtil.holiday.TestFestival;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 李朔
 * @version 1.0
 */

public class deom {
    public static void main(String[] args) {
        List<Holiday> list =new ArrayList<>();
        list.add(SpringFestival.getSpringFestival());
        list.add(TeacherFestival.getTeacherFestival());
        list.add(TestFestival.getTestFestival());
        CriteriaStudent criteriaStudent=new CriteriaStudent();
        System.out.println(criteriaStudent.meetCriteria(list));
        CriteriaTeacher criteriaTeacher=new CriteriaTeacher();
        System.out.println(criteriaTeacher.meetCriteria(list));
    }
}
