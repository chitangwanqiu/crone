package cronCreationAndParse.holidayUtil.holiday;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 李朔
 * @version 1.0
 * 单例设计模式-饿汉式
 */
public class TestFestival implements Holiday {
    private String startDate="-12-20 00:00:00";
    private String endDate="-12-20 23:59:59";
    private static TestFestival testFestival=new TestFestival();
    //构造器私有化
    private TestFestival(){ }
    //获取单例对象
    public static TestFestival getTestFestival(){
        return testFestival;
    }
    @Override
    public Date firstDayAfterFestival(){
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s=sdf.format(date);
        String dayString=s.substring(0,4)+"-12-21 00:00:00";
        Date parse= null;
        try {
            parse = sdf.parse(dayString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }
    @Override
    public Date getStartDate(){
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s=sdf.format(date);
        String dayString=s.substring(0,4)+startDate;
        Date parse= null;
        try {
            parse = sdf.parse(dayString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }
    @Override
    public Date getEndDate() {
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s=sdf.format(date);
        String dayString=s.substring(0,4)+endDate;
        Date parse= null;
        try {
            parse = sdf.parse(dayString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }

    @Override
    public String toString() {
        return "TestFestival{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
