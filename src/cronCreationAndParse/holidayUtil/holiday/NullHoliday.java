package cronCreationAndParse.holidayUtil.holiday;

import java.util.Date;

/**
 * @author 李朔
 * @version 1.0
 */
public class NullHoliday implements Holiday {

    @Override
    public Date firstDayAfterFestival() {
        return null;
    }

    @Override
    public Date getStartDate() {
        return null;
    }

    @Override
    public Date getEndDate() {
        return null;
    }
}
