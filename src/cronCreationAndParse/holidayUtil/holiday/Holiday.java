package cronCreationAndParse.holidayUtil.holiday;

import java.util.Date;

/**
 * @author 李朔
 * @version 1.0
 */
public  interface Holiday {

    public Date firstDayAfterFestival();
    public Date getStartDate();
    public Date getEndDate();

}
