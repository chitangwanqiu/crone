package cronCreationAndParse.holidayUtil.holiday;

/**
 * @author 李朔
 * @version 1.0
 * 简单工厂模式
 */
public class HolidayFactory {

    public static final Holiday[] holidays={SpringFestival.getSpringFestival(),TeacherFestival.getTeacherFestival(),TestFestival.getTestFestival()};
    public static Holiday getHoliday(Holiday holiday) {
        for(int i=0;i< holidays.length;i++){
            if(holidays[i].getClass().equals(holiday.getClass())){
               return holidays[i];
            }
        }
        return new NullHoliday();
    }
}
