package cronCreationAndParse.cronCreation;

/**
 * @author 李朔
 * @version 1.0
 */
public class CronCreationNoAddYear implements CronCreationStrategy {
    @Override
    public String cronCreation(String sec_str,String min_str,String hour_str,String day_str,String month_str,String week_str,String year_str){
        return sec_str+"  "+min_str+"  "+hour_str+"  "+day_str+"  "+month_str+"  "+week_str;
    }
    /**
     * 检查week和day字段的互斥属性
     * 格式检验函数
     * */
    public boolean checkWeekDay(boolean weekSelected,boolean daySelected){
        if(weekSelected || daySelected){
            if(weekSelected && daySelected){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
}
