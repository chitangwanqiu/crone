package cronCreationAndParse.cronCreation;

/**
 * @author 李朔
 * @version 1.0
 * 策略模式接口
 */
public interface CronCreationStrategy {

    public String cronCreation(String sec_str,String min_str,String hour_str,String day_str,String month_str,String week_str,String year_str);
}
