package cronCreationAndParse.cronCreation;

/**
 * @author 李朔
 * @version 1.0
 */
public class Context {
    private CronCreationStrategy cronCreationStrategy;
    //构造器
    public Context(CronCreationStrategy cronCreationStrategy){
        this.cronCreationStrategy=cronCreationStrategy;
    }
    public String executeStrategy(String sec_str,String min_str,String hour_str,String day_str,String month_str,String week_str,String year_str){
        return cronCreationStrategy.cronCreation(sec_str,min_str,hour_str,day_str,month_str,week_str,year_str);
    }
}
