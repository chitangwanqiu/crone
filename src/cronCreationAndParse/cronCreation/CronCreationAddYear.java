package cronCreationAndParse.cronCreation;

public class CronCreationAddYear implements CronCreationStrategy{
    /**
    * 根据UI界面上读取的各个值，生成相对应的cron表达式
    * 这里面的参数就是各个对应的域中得到的字符串
    * */
    @Override
    public String cronCreation(String sec_str,String min_str,String hour_str,String day_str,String month_str,String week_str,String year_str){
        return sec_str+"  "+min_str+"  "+hour_str+"  "+day_str+"  "+month_str+"  "+week_str+"  "+year_str;
    }
    /**
     * 检查week和day字段的互斥属性
     * 格式检验函数
     * */
    public boolean checkWeekDay(boolean weekSelected,boolean daySelected){
        if(weekSelected || daySelected){
            if(weekSelected && daySelected){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
}
