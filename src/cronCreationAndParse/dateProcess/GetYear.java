package cronCreationAndParse.dateProcess;

import java.util.Calendar;

public class GetYear {
    public static void main(String[] args) {
        System.out.println(new GetYear().getCurYear());
    }
    /**
     * 获取当前年份
     * */
    public int getCurYear(){
        Calendar nowCalendar=Calendar.getInstance();
        int YearNow=nowCalendar.get(nowCalendar.YEAR);
        return YearNow;
    }
}
