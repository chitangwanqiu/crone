package cronCreationAndParse.dateProcess;
//日历包
import java.util.Calendar;

public class GetDay {
    //属性
    private static final int[] lastDayOfMonth={31,29,31,30,31,30,31,31,30,31,30,31};
    //方法
    /**
     * 获取本月最后一天
     * */
    public int getLastDayOfNowMonth(){
        Calendar nowCalendar=Calendar.getInstance();
        int monthNow=nowCalendar.get(nowCalendar.MONTH)+1;
        return lastDayOfMonth[monthNow-1];
    }

}