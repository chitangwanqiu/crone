package cronCreationAndParse.dateProcess;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GetCurTime {

    public static void main(String[] args) {
        new GetCurTime().getCurTime();
    }
    /**
     * 获取当前时间
     * */
    public String getCurTime(){
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
        //format函数之后，类型为字符串类型
        return df.format(new Date());
    }
}
