package cronCreationAndParse.cronParse.parseDescStr.parseDesc;
import cronCreationAndParse.cronParse.parseDescStr.everyField.*;

/**
 * @author 李朔
 * @version 1.0
 */
public class ParseStrDescAddYear extends ParseStrDesc{
    @Override
    public String getDayParseStr(String fieldStr) {
        return new DayField().parseField(fieldStr);
    }
    @Override
    public String getSecParseStr(String fieldStr) {
        return new SecField().parseField(fieldStr);
    }
    @Override
    public String getMinuteParseStr(String fieldStr) {
        return new MinuteField().parseField(fieldStr);
    }
    @Override
    public String getHourParseStr(String fieldStr) {
        return new HourField().parseField(fieldStr);
    }
    @Override
    public String getMonthParseStr(String fieldStr) {
        return new MonthField().parseField(fieldStr);
    }
    @Override
    public String getYearParseStr(String fieldStr) {
        return new YearField().parseField(fieldStr);
    }
    @Override
    public String getWeekParseStr(String fieldStr) {
        return new WeekField().parseField(fieldStr);
    }
}
