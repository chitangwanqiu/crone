package cronCreationAndParse.cronParse.parseDescStr.parseDesc;

/**
 * @author 李朔
 * @version 1.0
 */
public abstract class ParseStrDesc {

    public abstract String getDayParseStr(String fieldStr);
    public abstract String getSecParseStr(String fieldStr);
    public abstract String getMinuteParseStr(String fieldStr);
    public abstract String getHourParseStr(String fieldStr);
    public abstract String getMonthParseStr(String fieldStr);
    public abstract String getYearParseStr(String fieldStr);
    public abstract String getWeekParseStr(String fieldStr);

    public String getParseStr(String dayFieldStr,String secFieldStr,String minuteFieldStr,String hourFieldStr,String yearFieldStr,String monthFieldStr,String weekFieldStr){
        String DayStr=getDayParseStr(dayFieldStr);
        String SecStr=getSecParseStr(secFieldStr);
        String MinuteStr=getMinuteParseStr(minuteFieldStr);
        String HourStr=getHourParseStr(hourFieldStr);
        String YearStr=getYearParseStr(yearFieldStr);
        String MonthStr=getMonthParseStr(monthFieldStr);
        String Weekstr=getWeekParseStr(weekFieldStr);
        return SecStr+MinuteStr+HourStr+DayStr+MonthStr+YearStr+Weekstr;
    }

    public String getParseStr(String[] fieldStrs){
        String DayStr=getDayParseStr(fieldStrs[0]);
        String SecStr=getSecParseStr(fieldStrs[1]);
        String MinuteStr=getMinuteParseStr(fieldStrs[2]);
        String HourStr=getHourParseStr(fieldStrs[3]);
        String YearStr=getYearParseStr(fieldStrs[4]);
        String MonthStr=getMonthParseStr(fieldStrs[5]);
        String Weekstr=getWeekParseStr(fieldStrs[6]);
        return SecStr+" "+MinuteStr+" "+HourStr+" "+DayStr+" "+MonthStr+" "+YearStr+" "+Weekstr;
    }
}
