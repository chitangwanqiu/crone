package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class SecField{
    //属性
    //允许使用的特殊字符

    private static final char[] signal_={'*','\\','-',','};
    //区域解析
    /**
     * 秒部分的cron解析函数
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*") && fieldStr.contains("\\")){
            //包含"*",且包含"\"
            return "每隔"+fieldStr.substring(2)+"秒";

        }
        else if(fieldStr.contains("*")){
            //每秒发生一次
            return "每秒";
        }
        else if(fieldStr.contains("\\")){
            return "从第"+fieldStr.substring(0,fieldStr.indexOf('\\'))+"秒开始，每隔"+fieldStr.substring(fieldStr.indexOf('\\')+1)+"秒";
        }
        else if(fieldStr.contains("-")){
            //时间间隔
//            String sec_process=sec_part.split("-");
            return "从第"+fieldStr.substring(0,fieldStr.indexOf('-'))+"秒开始到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"秒为止";
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="在第";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1]+"秒";
        }
        else{
            return "第"+fieldStr+"秒";
        }
    }
}
