package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class MonthField{
    /**
     * 月部分的cron解析函数
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*")){
            //每天发生
            return "每月";
        }
        else if(fieldStr.contains("\\")){
            return "从"+fieldStr.substring(0,fieldStr.indexOf('\\'))+"月份开始，每隔"+fieldStr.substring(fieldStr.indexOf('\\')+1)+"个月";
        }
        else if(fieldStr.contains("-")){
            //时间间隔
//            String sec_process=sec_part.split("-");
            return "从"+fieldStr.substring(0,fieldStr.indexOf('-'))+"月份开始到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"月份的";
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="在";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1]+"月份";
        }
        else if(fieldStr.contains("?")){
            return "";
        }
        else{
            return fieldStr+"月份";
        }
    }
}
