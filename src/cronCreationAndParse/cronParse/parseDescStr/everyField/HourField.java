package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class HourField {
    /**
     * hourField部分的cron解析函数
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*") && fieldStr.contains("\\")){
            //包含"*",且包含"\"
//            String str_process=sec_part.split();
            return "每隔"+fieldStr.substring(2)+"小时";

        }
        else if(fieldStr.contains("*")){
            //每秒发生一次
            return "每个小时";
        }
        else if(fieldStr.contains("\\")){
            return "从"+fieldStr.substring(0,fieldStr.indexOf('\\'))+"点起，每隔"+fieldStr.substring(fieldStr.indexOf('\\')+1)+"个小时";
        }
        else if(fieldStr.contains("-")){
            //时间间隔
//            String sec_process=sec_part.split("-");
            return fieldStr.substring(0,fieldStr.indexOf('-'))+"点到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"点";
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="在";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1]+"点";
        }
        else{
            return fieldStr+"点";
        }
    }
}
