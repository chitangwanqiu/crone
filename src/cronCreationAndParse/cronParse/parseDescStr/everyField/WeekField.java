package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class WeekField {
    //解析函数
    /**
     * weekField解析
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*")){
            //每秒发生一次
            return "每周";
        }
        else if(fieldStr.contains("#")){
            return "第"+fieldStr.substring(0,fieldStr.indexOf('#'))+"周的星期"+fieldStr.substring(fieldStr.indexOf('#')+1);
        }
        else if(fieldStr.contains("-")){
            //时间间隔
            return "从星期"+fieldStr.substring(0,fieldStr.indexOf('-'))+"到星期"+fieldStr.substring(fieldStr.indexOf('-')+1);
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="周";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1];
        }
        else if(fieldStr.contains("?")){
            return "";
        }
        else{
            return "周"+fieldStr;
        }
    }

}
