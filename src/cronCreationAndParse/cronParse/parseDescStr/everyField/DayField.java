package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class DayField{
    /**
     * DayField部分的cron解析函数
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*") && fieldStr.contains("\\")){
            //包含"*",且包含"\"
//            String str_process=sec_part.split();
            return "每隔"+fieldStr.substring(2)+"天";
        }
        else if(fieldStr.contains("*")){
            //每天发生
            return "每天";
        }
        else if(fieldStr.contains("\\")){
            return "从第"+fieldStr.substring(0,fieldStr.indexOf('\\'))+"天开始，每隔"+fieldStr.substring(fieldStr.indexOf('\\')+1)+"天";
        }
        else if(fieldStr.contains("-")){
            //时间间隔
//            String sec_process=sec_part.split("-");
            return "从第"+fieldStr.substring(0,fieldStr.indexOf('-'))+"天开始到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"天的";
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="在第";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1]+"天";
        }
        else if(fieldStr.contains("L")){
            //
            return "最后一天";
        }
        else if(fieldStr.contains("?")){
            //如果符号是?，就把天的概念淡化了
            return "";
        }
        else{
            return "第"+fieldStr+"天";
        }
    }
}
