package cronCreationAndParse.cronParse.parseDescStr.everyField;


import cronCreationAndParse.dateProcess.GetYear;

public class YearField{
    //区域表达式串破解
    /**
     * YearField解析
     * */
    public String parseField(String fieldStr){
        if(fieldStr.equals("*")){
            //全选，表示每一年
            String yearField;
            int yearFieldInt=new GetYear().getCurYear();
            yearField=yearFieldInt+"";
            return "从"+yearField+"年开始，以后每年 ";
        }
        else{
            //指定范围
            return "从"+fieldStr.substring(0,fieldStr.indexOf('-'))+"年开始到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"年 ";
        }
    }
}
