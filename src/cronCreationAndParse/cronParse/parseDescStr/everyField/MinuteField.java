package cronCreationAndParse.cronParse.parseDescStr.everyField;

public class MinuteField {
    /**
     * 分钟部分的cron解析函数
     * */
    public String parseField(String fieldStr){
        if(fieldStr.contains("*") && fieldStr.contains("\\")){
            //包含"*",且包含"\"
            return "每隔"+fieldStr.substring(2)+"分";
        }
        else if(fieldStr.contains("*")){
            return "每分";
        }
        else if(fieldStr.contains("\\")){
            return "从"+fieldStr.substring(0,fieldStr.indexOf('\\'))+"分开始，每隔"+fieldStr.substring(fieldStr.indexOf('\\')+1)+"分 ";
        }
        else if(fieldStr.contains("-")){
            //时间间隔
            return "从"+fieldStr.substring(0,fieldStr.indexOf('-'))+"分到"+fieldStr.substring(fieldStr.indexOf('-')+1)+"分的";
        }
        else if(fieldStr.contains(",")){
            String[] str_process=fieldStr.split(",");
            String res="在第";
            for(int i=0;i< str_process.length-1;++i){
                res+=str_process[i]+"、";
            }
            return res+str_process[str_process.length-1]+"分";
        }
        else{
            return "第"+fieldStr+"分";
        }
    }
}
