package cronCreationAndParse.cronParse.parseNextTime;
import java.util.Date;
/**
 * @author 李朔
 * @version 1.0
 */
public abstract class CronParseDecorator implements CronParse {
    protected CronParse decoratedCronParse;
    //构造器
    public CronParseDecorator(CronParse decoratedCronParse){

        this.decoratedCronParse=decoratedCronParse;
    }
    @Override
    public Date next(Date date) {
        return decoratedCronParse.next(date);
    }
}
