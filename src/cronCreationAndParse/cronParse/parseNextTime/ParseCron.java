package cronCreationAndParse.cronParse.parseNextTime;


import cronCreationAndParse.cronParse.parseDescStr.everyField.*;

public class ParseCron {

    public static void main(String[] args) {
        new ParseCron().parseCron("0 0-5 2 * * ? 2021-2022");
    }
    /**
     * 解析cron表达式含义
     * @param cronStr cron表达式
     * */
    public String parseCron(String cronStr){
        String[] fieldStrs=cronStr.split(" ");
        if(fieldStrs.length==6){
        }
        else if(fieldStrs.length==7){
            YearField yearField=new YearField();
            String yearStr=yearField.parseField(fieldStrs[6]);
            String secStr=new SecField().parseField(fieldStrs[0]);
            String minuteStr=new MinuteField().parseField(fieldStrs[1]);
            String hourStr=new HourField().parseField(fieldStrs[2]);
            String dayStr=new DayField().parseField(fieldStrs[3]);
            String monthStr=new MonthField().parseField(fieldStrs[4]);
            String weekStr=new WeekField().parseField(fieldStrs[5]);
            System.out.println(yearStr+" "+weekStr+" "+monthStr+" "+dayStr+" "+hourStr+" "+minuteStr+secStr);
            return yearStr;
        }
        else{
            System.out.println("输入的cron表达式不正确");
        }
        return "";
    }
}
