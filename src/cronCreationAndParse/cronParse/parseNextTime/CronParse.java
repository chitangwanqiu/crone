package cronCreationAndParse.cronParse.parseNextTime;

import java.util.Date;

/**
 * @author 李朔
 * @version 1.0
 */
public interface CronParse {

    public abstract Date next(Date date);
}
