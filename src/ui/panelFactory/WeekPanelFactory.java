package ui.panelFactory;



import ui.everyPanel.WeekPanel;

import javax.swing.*;

/**
 * @author 李朔
 * @version 1.0
 * 用于创建WeekPanel类实例的工厂
 */
public class WeekPanelFactory implements PanelFactory{
    @Override
    public JPanel factory(){
        return new WeekPanel();
    }
}
