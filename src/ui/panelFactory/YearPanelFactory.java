package ui.panelFactory;


import ui.everyPanel.YearPanel;

import javax.swing.*;

/**
 * @author 李朔
 * @version 1.0
 * 用于创建YearPanel类实例的工厂
 */
public class YearPanelFactory implements PanelFactory{
    @Override
    public JPanel factory(){
        return new YearPanel();
    }
}
