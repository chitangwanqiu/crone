package ui.panelFactory;


import ui.everyPanel.HourPanel;

import javax.swing.*;

/**
 * @author 李朔
 * @version 1.0
 * 用于创建HourPanel类实例的工厂
 */
public class HourPanelFactory implements PanelFactory{
        @Override
        public JPanel factory(){
            return new HourPanel();
        }
}
