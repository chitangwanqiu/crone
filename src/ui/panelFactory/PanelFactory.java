package ui.panelFactory;

import javax.swing.*;

/**
 * @author 李朔
 * @version 1.0
 * 多态工厂模式-PanelFactory接口
 */
public interface PanelFactory {


    public abstract JPanel factory();
}
