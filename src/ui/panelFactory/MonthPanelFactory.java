package ui.panelFactory;



import ui.everyPanel.MonthPanel;

import javax.swing.*;

/**
 * @author 李朔
 * @version 1.0
 * 用于创建MonthPanel类实例的工厂
 */
public class MonthPanelFactory implements PanelFactory {
    @Override
    public JPanel factory(){
        return new MonthPanel();
    }
}
