package ui.panelFactory;
import ui.everyPanel.DayPanel;
import javax.swing.*;
/**
 * @author 李朔
 * @version 1.0
 * 用于创建DayPanel类实例对象的工厂
 */
public class DayPanelFactory implements PanelFactory{
    @Override
    public JPanel factory(){
        return new DayPanel();
    }
}
