package ui.everyPanel;



import javax.swing.*;
import java.awt.*;

public class SecAndMinutePanel extends JPanel implements PanelInterface {
    //属性
    //画板，五个
    private JPanel jp1, jp2, jp3,jp4,jp5;
    //标签
    private JLabel jlb1, jlb2,jlb3,jlb4;
    private JLabel jlb31,jlb32,jlb33;
    private JLabel jlb41,jlb42,jlb43;
    private JTextField jtx31,jtx32;
    private JTextField jtx41,jtx42;
    //复选框
    private JCheckBox[] jcbs;
    private JRadioButton jrb1, jrb2,jrb3,jrb4;
    @Override
    public void createPanel(){
        jcbs=new JCheckBox[60];
        //创建面板
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jp4 = new JPanel();
        jp5 = new JPanel();
        //创建标签
        jlb1 = new JLabel("指定：");
        jlb2 = new JLabel("单选框:");
        jlb3=new JLabel("范围:");
        jlb4=new JLabel("间隔");

        jlb1.setFont(new Font("宋体",Font.BOLD, 16));
        jlb2.setFont(new Font("宋体",Font.BOLD, 16));
        jlb3.setFont(new Font("宋体",Font.BOLD, 16));
        jlb4.setFont(new Font("宋体",Font.BOLD, 16));
//        jlb1.setPreferredSize(new Dimension(100,30));
//        jlb2.setPreferredSize(new Dimension(50,40));
        //复选框名称
        for(int i=0;i<60;i++){
            jcbs[i]=new JCheckBox(""+i);
            jcbs[i].setFont(new Font("宋体",Font.BOLD, 16));
        }
        // 下面可以设置单选
        ButtonGroup bg2=new ButtonGroup();
        //单选框
        jrb1 = new JRadioButton("All");
        jrb2 = new JRadioButton("范围");
        jrb3=new JRadioButton("间隔");
        jrb4=new JRadioButton("指定");

        jrb1.setFont(new Font("宋体",Font.BOLD, 16));
        jrb2.setFont(new Font("宋体",Font.BOLD, 16));
        jrb3.setFont(new Font("宋体",Font.BOLD, 16));
        jrb4.setFont(new Font("宋体",Font.BOLD, 16));
        // 一定要把jrb1，jrb2放入到一个ButtonGroup里面
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrb1);
        bg.add(jrb2);
        bg.add(jrb3);
        bg.add(jrb4);
        //设置默认选择
        jrb1.setSelected(true);
        this.setLayout(new GridLayout(3, 1));
        jp1.add(jlb1);
        for(int i=0;i<60;++i){
            jp1.add(jcbs[i]);
        }
        jp2.add(jlb2);
        jp2.add(jrb1);
        jp2.add(jrb2);
        jp2.add(jrb3);
        jp2.add(jrb4);
        jp3.add(jlb3);

        jlb31=new JLabel("周期从");
        jtx31=new JTextField(3);
        jlb32=new JLabel("-");
        jtx32=new JTextField(3);

        jlb31.setFont(new Font("宋体",Font.BOLD, 16));
        jtx31.setFont(new Font("宋体",Font.BOLD, 16));
        jlb32.setFont(new Font("宋体",Font.BOLD, 16));
        jtx32.setFont(new Font("宋体",Font.BOLD, 16));

        jp3.add(jlb31);
        jp3.add(jtx31);
        jp3.add(jlb32);
        jp3.add(jtx32);

        jlb41=new JLabel("从");

        jtx41=new JTextField(3);
        jtx42=new JTextField(3);

        jlb41.setFont(new Font("宋体",Font.BOLD, 16));

        jtx41.setFont(new Font("宋体",Font.BOLD, 16));
        jtx42.setFont(new Font("宋体",Font.BOLD, 16));
        jp4.add(jlb41);
        jp4.add(jtx41);
        jp4.add(jtx42);

//        jp1.setPreferredSize(new Dimension(900,200));
        // 加入到JFrame
        setLayout(new GridLayout(4,1,2,0));

    }
    //构造器
    /**
     * SecPanel和MinutePanel两个部分的布局
     * */
    public SecAndMinutePanel(String section) {
        createPanel();
        jlb33=new JLabel(section);
        jlb33.setFont(new Font("宋体",Font.BOLD, 16));
        jp3.add(jlb33);
        jlb42=new JLabel(section+"开始，每");
        jlb43=new JLabel(section+"执行一次");
        jlb42.setFont(new Font("宋体",Font.BOLD, 16));
        jlb43.setFont(new Font("宋体",Font.BOLD, 16));
        jp4.add(jlb42);
        jp4.add(jlb43);
        this.add(jp1);
        this.add(jp2);
        this.add(jp3);
        this.add(jp4);
    }
    //方法
    /**
     * 检查分钟输入信息格式和秒输入信息格式
     * */
    public boolean check_sec_minute_info(SecAndMinutePanel secAndMinute){
        if(secAndMinute.jrb4.isSelected()){
            //指定
            for(int i=0;i<60;++i){
                if(secAndMinute.jcbs[i].isSelected()){
                    return true;
                }
            }
            return false;
        }
        else if(secAndMinute.jrb2.isSelected()){
            //范围
            if(secAndMinute.jtx31.getText().equals("") || secAndMinute.jtx32.getText().equals("")) {
                //未输入数据，返回false
                return false;
            }
            int tx1=Integer.parseInt(secAndMinute.jtx31.getText());
            int tx2=Integer.parseInt(secAndMinute.jtx32.getText());
            if(tx1<tx2 && tx1<=59 && tx1>=0 && tx2>=0 &&tx2<=59){
                return true;
            }
            else{
                //不符合条件
                System.out.println("解析出错");
                return false;
            }
        }
        else if(secAndMinute.jrb3.isSelected()){
            if(secAndMinute.jtx42.getText().equals("")||secAndMinute.jtx41.getText().equals("")){
                //未输入完全
                return false;
            }
            int tx1=Integer.parseInt(secAndMinute.jtx41.getText());
            if(tx1>=0 &&tx1<=59) {
                return true;
            }
            else{
                System.out.println("范围有问题");
                return false;
            }
        }
        else {
            return true;
        }
    }
    /**
     * 得到对应两部分信息的cron表达式
     * */
    public String get_sec_minute_str(SecAndMinutePanel secAndMinute){
        String sec_str="";
        if(secAndMinute.jrb4.isSelected()){
            int is_first=0;
            for(int i=0;i<60;++i){
                if(secAndMinute.jcbs[i].isSelected()){
                    if(is_first<=0){
                        is_first+=1;
                        sec_str+=secAndMinute.jcbs[i].getText();
                    }else{
                        sec_str+=","+secAndMinute.jcbs[i].getText();
                    }
                }
            }
        }
        else if(secAndMinute.jrb2.isSelected()){
            //范围
//            System.out.println("范围");
            int tx1=Integer.parseInt(secAndMinute.jtx31.getText());
            int tx2=Integer.parseInt(secAndMinute.jtx32.getText());
            if(tx1<tx2 && tx1<=59 && tx1>=0 && tx2>=0 &&tx2<=59){
                sec_str=secAndMinute.jtx31.getText()+"-"+secAndMinute.jtx32.getText();
            }
            //System.out.println(sec_str);
            else{
                //不符合条件
                System.out.println("解析出错");
                sec_str="￥";
            }
        }
        else if(secAndMinute.jrb3.isSelected()){
//            System.out.println("间隔");
            int tx1=Integer.parseInt(secAndMinute.jtx41.getText());
            if(tx1>=0 &&tx1<=59) {
                sec_str = secAndMinute.jtx41.getText() + "\\" + secAndMinute.jtx42.getText();
            }else{
                System.out.println("范围有问题");
//                JOptionPane.showMessageDialog(null, "输入有误");
//                JOptionPane.showMessageDialog(null, "alert", "alert", JOptionPane.ERROR_MESSAGE);
                sec_str="!";
            }
//            System.out.println(sec_str);
        }
        else {
            sec_str = "*";
        }
        return sec_str;
    }
}
