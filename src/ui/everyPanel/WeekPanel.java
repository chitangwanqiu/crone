package ui.everyPanel;



import javax.swing.*;
import java.awt.*;

public class WeekPanel extends JPanel implements PanelInterface {
    //属性
    private JPanel jp1, jp2, jp3,jp4,jp5;
    private JLabel jlb1, jlb2,jlb3,jlb4;
    private JLabel jlb31,jlb32,jlb33;
    private JLabel jlb41,jlb42,jlb43;
    private JTextField jtx31,jtx32;
    private JTextField jtx41,jtx42;
    private JCheckBox[] jcbs;
    private JRadioButton jrb1, jrb2,jrb3,jrb4,jrb5;
    //重写方法
    @Override
    public void createPanel(){
        jcbs=new JCheckBox[7];
        //创建面板
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jp4 = new JPanel();
        jp5 = new JPanel();
        //创建标签
        jlb1 = new JLabel("指定：");
        jlb2 = new JLabel("单选框:");
        jlb3=new JLabel("范围:");
        jlb4=new JLabel("间隔");
        //复选框名称
        for(int i=1;i<=7;i++){
            jcbs[i-1]=new JCheckBox(""+i);
        }
        // 下面可以设置单选
        ButtonGroup bg2=new ButtonGroup();
        //单选框
        jrb1 = new JRadioButton("All");
        jrb2 = new JRadioButton("范围");
        jrb3=new JRadioButton("间隔");
        jrb4=new JRadioButton("指定");
        jrb5=new JRadioButton("不指定");
        // 一定要把jrb1，jrb2放入到一个ButtonGroup里面
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrb1);
        bg.add(jrb2);
        bg.add(jrb3);
        bg.add(jrb4);
        bg.add(jrb5);
        jrb5.setSelected(true);
        this.setLayout(new GridLayout(3, 1));
        jp1.add(jlb1);
        for(int i=0;i<7;++i){
            jp1.add(jcbs[i]);
        }
        jp2.add(jlb2);
        jp2.add(jrb1);
        jp2.add(jrb2);
        jp2.add(jrb3);
        jp2.add(jrb4);
        jp2.add(jrb5);
        jp3.add(jlb3);
        jlb31=new JLabel("周期从星期");
        jtx31=new JTextField(3);
        jlb32=new JLabel("- 星期");
        jtx32=new JTextField(3);
        jlb33=new JLabel("");
        jp3.add(jlb31);
        jp3.add(jtx31);
        jp3.add(jlb32);
        jp3.add(jtx32);
        jp3.add(jlb33);
        jlb41=new JLabel("第");
        jlb42=new JLabel("周的");
        jlb43=new JLabel("星期");
        jtx41=new JTextField(3);
        jtx42=new JTextField(3);
        jp4.add(jlb41);
        jp4.add(jtx41);
        jp4.add(jlb42);
        jp4.add(jtx42);
        jp4.add(jlb43);
        // 加入到JFrame
        setLayout(new GridLayout(4,1));
        this.add(jp1);
        this.add(jp2);
        this.add(jp3);
        this.add(jp4);
    }
    //构造器
    /**
     * WeekPanel布局
     * */
    public WeekPanel() {
        createPanel();
    }
    /**
     *得到对应该部分输入的cron表达式
     **/
    public String get_week_str(WeekPanel week){
        String week_str="";
        if(week.jrb1.isSelected()){
            //如果单选框中，第一个单选项被选中了
            //all
//            System.out.println("*");
            week_str="*";
        }
        else if(week.jrb2.isSelected()){
            //范围
            week_str=week.jtx31.getText()+"-"+week.jtx32.getText();
        }
        else if(week.jrb3.isSelected()){
            //间隔
            week_str=week.jtx41.getText()+"#"+week.jtx42.getText();
        }
        else{
            //不指定的情况下
            week_str="?";
        }
        return week_str;
    }
    /**
     * 检查week_str信息格式
     * */
    public boolean check_week_str(WeekPanel week){
        if(week.jrb1.isSelected()){
            //全选
            return true;
        }
        else if(week.jrb2.isSelected()){
            //范围
            //从星期几到星期几
            if(week.jtx31.getText().equals("") || week.jtx32.getText().equals("")){
                return false;
            }
            int tx1=Integer.getInteger(week.jtx31.getText()),tx2=Integer.getInteger(week.jtx32.getText());
            if(tx1>=1 && tx1<6 &&tx2>=1 &&tx2<=7 && tx1<tx2){
                return true;
            }
            else if(week.jrb3.isSelected()){
                //间隔
                if(week.jtx41.getText().equals("") ||week.jtx42.getText().equals("")){
                    return false;
                }
                int tx41=Integer.getInteger(week.jtx41.getText()),tx42=Integer.getInteger(week.jtx42.getText());
                if(tx41>=1 &&tx41<=4 && tx42>=1 && tx42<=7){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else {
            //不指定的情况下
            return true;
        }
    }
    /**
     * 返回week部分是否为未指定
     * */
    public boolean getJrb5SelectedInfo(){
        if(jrb5.isSelected()){
            return true;
        }
        else{
            return false;
        }
    }
}
