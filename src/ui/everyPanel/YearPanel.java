package ui.everyPanel;



import javax.swing.*;
import java.awt.*;

public class YearPanel extends JPanel implements PanelInterface {
    //属性
    private JPanel jp1, jp2, jp3;
    private JLabel jlb1, jlb2,jlb3,jlb4;
    private JLabel jlb31,jlb32,jlb33;
    private JTextField jtx31,jtx32;
    private JRadioButton jrb1, jrb2,jrb3;
    @Override
    public void createPanel(){
        //创建面板
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jlb2 = new JLabel("单选框:");
        jlb4=new JLabel("间隔");
        ButtonGroup bg2=new ButtonGroup();
        //单选框
        jrb1 = new JRadioButton("All");
        jrb2 = new JRadioButton("范围");
        jrb3=new JRadioButton("不指定");
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrb1);
        bg.add(jrb2);
        bg.add(jrb3);
        jrb3.setSelected(true);
        this.setLayout(new GridLayout(3, 1));
        jp1.add(jlb2);
        jp1.add(jrb1);
        jp1.add(jrb2);
        jp1.add(jrb3);
        jlb31=new JLabel("周期从");
        jtx31=new JTextField(3);
        jlb32=new JLabel("-");
        jtx32=new JTextField(3);
        jlb33=new JLabel("年");
        jp2.add(jlb31);
        jp2.add(jtx31);
        jp2.add(jlb32);
        jp2.add(jtx32);
        jp2.add(jlb33);
        // 加入到JFrame
        setLayout(new GridLayout(3,1));
        this.add(jp1);
        this.add(jp2);
        this.add(jp3);
    }
    //构造器
    /**
     * YearPanel布局
     * */
    public YearPanel() {
        createPanel();
    }
    //方法
    /**
     * 得到对应year部分输入的cron表达式
     * */
    public String get_year_str(YearPanel year){
        String year_str="";
        if(year.jrb1.isSelected()){
            //如果单选框中，第一个单选项被选中了
            //all
//            System.out.println("*");
            year_str="*";
        }
        else if(year.jrb2.isSelected()){
            //范围
//            System.out.println("范围");
            year_str=year.jtx31.getText()+"-"+year.jtx32.getText();
//            System.out.println(year_str);
        }
        else{
            year_str="";
        }
        return year_str;
    }
    /**
     * 检查输入格式
     **/
    public boolean check_year_str(YearPanel year){
        if(year.jrb2.isSelected()){
            if (year.jtx31.getText().equals("") || year.jtx32.getText().equals("")) {

                return false;
            }
            int tx1=Integer.getInteger(year.jtx31.getText()),tx2=Integer.getInteger(year.jtx32.getText());
            if(tx1>=2021 && tx2>2021 && tx2>tx1){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }
}
