package ui.everyPanel;


import javax.swing.*;
import java.awt.*;

public class HourPanel extends JPanel implements PanelInterface {
   //属性
   private JPanel jp1, jp2, jp3,jp4,jp5;
   private JLabel jlb1, jlb2,jlb3,jlb4;
   private JLabel jlb31,jlb32,jlb33;
   private JLabel jlb41,jlb42,jlb43;
   private JTextField jtx31,jtx32;
   private JTextField jtx41,jtx42;
   private JCheckBox[] jcbs;
   private JRadioButton jrb1, jrb2,jrb3,jrb4;
   @Override
   /**
    * 该部分模块场景构建
    * */
   public void createPanel(){
       jcbs=new JCheckBox[24];
       //创建面板
       jp1 = new JPanel();
       jp2 = new JPanel();
       jp3 = new JPanel();
       jp4 = new JPanel();
       jp5 = new JPanel();
       //创建标签
       jlb1 = new JLabel("指定：");
       jlb2 = new JLabel("单选框:");
       jlb3=new JLabel("范围:");
       jlb4=new JLabel("间隔");

       jlb1.setFont(new Font("宋体",Font.BOLD, 16));
       jlb2.setFont(new Font("宋体",Font.BOLD, 16));
       jlb3.setFont(new Font("宋体",Font.BOLD, 16));
       jlb4.setFont(new Font("宋体",Font.BOLD, 16));
       //复选框名称
       for(int i=1;i<=24;i++){
           jcbs[i-1]=new JCheckBox(""+i);
           jcbs[i-1].setFont(new Font("宋体",Font.BOLD, 16));
       }
       // 下面可以设置单选
       ButtonGroup bg2=new ButtonGroup();
       //单选框
       jrb1 = new JRadioButton("All");
       jrb2 = new JRadioButton("范围");
       jrb3=new JRadioButton("间隔");
       jrb4=new JRadioButton("指定");

       jrb1.setFont(new Font("宋体",Font.BOLD, 16));
       jrb2.setFont(new Font("宋体",Font.BOLD, 16));
       jrb3.setFont(new Font("宋体",Font.BOLD, 16));
       jrb4.setFont(new Font("宋体",Font.BOLD, 16));
       // 一定要把jrb1，jrb2放入到一个ButtonGroup里面
       ButtonGroup bg = new ButtonGroup();
       bg.add(jrb1);
       bg.add(jrb2);
       bg.add(jrb3);
       bg.add(jrb4);
       jrb1.setSelected(true);
       this.setLayout(new GridLayout(3, 1));
       jp1.add(jlb1);
       for(int i=0;i<24;++i){
           jp1.add(jcbs[i]);
       }
       jp2.add(jlb2);
       jp2.add(jrb1);
       jp2.add(jrb2);
       jp2.add(jrb3);
       jp2.add(jrb4);
       jp3.add(jlb3);
       jlb31=new JLabel("周期从");
       jtx31=new JTextField(3);
       jlb32=new JLabel("-");
       jtx32=new JTextField(3);
       jlb33=new JLabel("时");
       jlb31.setFont(new Font("宋体",Font.BOLD, 16));
       jtx31.setFont(new Font("宋体",Font.BOLD, 16));
       jlb32.setFont(new Font("宋体",Font.BOLD, 16));
       jtx32.setFont(new Font("宋体",Font.BOLD, 16));
       jlb33.setFont(new Font("宋体",Font.BOLD, 16));
       jp3.add(jlb31);
       jp3.add(jtx31);
       jp3.add(jlb32);
       jp3.add(jtx32);
       jp3.add(jlb33);
       jlb41=new JLabel("从");
       jlb42=new JLabel("时开始，每");
       jlb43=new JLabel("时执行一次");
       jtx41=new JTextField(3);
       jtx42=new JTextField(3);

       jlb41.setFont(new Font("宋体",Font.BOLD, 16));
       jlb42.setFont(new Font("宋体",Font.BOLD, 16));
       jlb43.setFont(new Font("宋体",Font.BOLD, 16));
       jtx41.setFont(new Font("宋体",Font.BOLD, 16));
       jtx42.setFont(new Font("宋体",Font.BOLD, 16));
       jp4.add(jlb41);
       jp4.add(jtx41);
       jp4.add(jlb42);
       jp4.add(jtx42);
       jp4.add(jlb43);
       //
       setLayout(new GridLayout(4,1));
       this.add(jp1);
       this.add(jp2);
       this.add(jp3);
       this.add(jp4);
   }
   //构造器
   /**
    * HourPanel布局搭建
    * */
     public HourPanel() {
        createPanel();
    }
   /**
    * 小时部分输入数据格式检验
    * * */
   public boolean check_hour_str(HourPanel hour){
        if(hour.jrb4.isSelected()){
             //指定模式下，如果存在被选中的项，则返回true
             for(int i=0;i<24;++i){
                  if(hour.jcbs[i].isSelected()){
                       return true;
                  }
             }
             //所有项都未被选中，则返回false
             return false;
        }
        else if(hour.jrb2.isSelected()){
             //范围选项
             if(hour.jtx31.getText().equals("") || hour.jtx32.getText().equals("")){
                  return false;
             }
             //符合范围要求，返回true
             int tx1=Integer.parseInt(hour.jtx31.getText());
             int tx2=Integer.parseInt(hour.jtx32.getText());
             if(tx1<tx2 && tx1<=59 && tx1>=0 && tx2>=0 &&tx2<=59){
                  return true;
             }
             else{
                  //不符合条件
                  System.out.println("解析出错");
                  return false;
             }
        }
        else if(hour.jrb3.isSelected()){
             //时间间隔的选项
             if(hour.jtx41.getText().equals("") ||hour.jtx42.getText().equals("")){
                  return false;
             }
             int tx1=Integer.parseInt(hour.jtx41.getText());
             if(tx1>=0 &&tx1<=59) {
                  //起始时间是正常的
                  return true;
             }
             else{
                  //起始时间超出范围
                  System.out.println("范围有问题");
                  return false;
             }
        }
        else {
             //all选项项，不需要进行额外的判断
             return true;
        }
   }
   /**
    * 获取小时部分输入数据
    * */
   public String get_hour_str(HourPanel hour){
        String hour_str="";
        if(hour.jrb4.isSelected()){
             int is_first=0;
             for(int i=0;i<24;++i){
                  if(hour.jcbs[i].isSelected()){
                       if(is_first<=0){
                            is_first+=1;
                            hour_str+=hour.jcbs[i].getText();
                       }else{
                            hour_str+=","+hour.jcbs[i].getText();
                       }
                  }
             }
        }
        else if(hour.jrb2.isSelected()){
             hour_str=hour.jtx31.getText()+"-"+hour.jtx32.getText();
        }
        else if(hour.jrb3.isSelected()){
//            System.out.println("间隔");
             hour_str=hour.jtx41.getText()+"\\"+hour.jtx42.getText();
        }
        else{
//            System.out.println("指定");
             hour_str="*";
//            System.out.println(sec_str);
        }
        return hour_str;
   }
}
