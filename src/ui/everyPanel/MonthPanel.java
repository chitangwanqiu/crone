package ui.everyPanel;


import javax.swing.*;
import java.awt.*;

public class MonthPanel extends JPanel implements PanelInterface {
    //属性
    private JPanel jp1, jp2, jp3,jp4,jp5;
    private JLabel jlb1, jlb2,jlb3,jlb4;
    private JLabel jlb31,jlb32,jlb33;
    private JLabel jlb41,jlb42,jlb43;
    private JTextField jtx31,jtx32;
    private JTextField jtx41,jtx42;
    private JCheckBox[] jcbs;
    private JRadioButton jrb1, jrb2,jrb3,jrb4,jrb5;
    @Override
    public void createPanel(){
        jcbs=new JCheckBox[12];
        //创建面板
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jp4 = new JPanel();
        jp5 = new JPanel();
        //创建标签
        jlb1 = new JLabel("指定：");
        jlb2 = new JLabel("单选框:");
        jlb3=new JLabel("范围:");
        jlb4=new JLabel("间隔");
        //复选框名称
        for(int i=1;i<=12;i++){
            jcbs[i-1]=new JCheckBox(""+i);
        }
        // 下面可以设置单选
        ButtonGroup bg2=new ButtonGroup();
        //单选框
        jrb1 = new JRadioButton("All");
        jrb2 = new JRadioButton("范围");
        jrb3=new JRadioButton("间隔");
        jrb4=new JRadioButton("指定");
        jrb5=new JRadioButton("不指定");
        // 一定要把jrb1，jrb2放入到一个ButtonGroup里面
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrb1);
        bg.add(jrb2);
        bg.add(jrb3);
        bg.add(jrb4);
        bg.add(jrb5);
        jrb1.setSelected(true);
        this.setLayout(new GridLayout(3, 1));
        jp1.add(jlb1);
        for(int i=0;i<12;++i){
            jp1.add(jcbs[i]);
        }
        jp2.add(jlb2);
        jp2.add(jrb1);
        jp2.add(jrb2);
        jp2.add(jrb3);
        jp2.add(jrb4);
        jp2.add(jrb5);
        jp3.add(jlb3);
        jlb31=new JLabel("周期从");
        jtx31=new JTextField(3);
        jlb32=new JLabel("-");
        jtx32=new JTextField(3);
        jlb33=new JLabel("月");
        jp3.add(jlb31);
        jp3.add(jtx31);
        jp3.add(jlb32);
        jp3.add(jtx32);
        jp3.add(jlb33);
        jlb41=new JLabel("从");
        jlb42=new JLabel("月开始，每");
        jlb43=new JLabel("月执行一次");
        jtx41=new JTextField(3);
        jtx42=new JTextField(3);
        jp4.add(jlb41);
        jp4.add(jtx41);
        jp4.add(jlb42);
        jp4.add(jtx42);
        jp4.add(jlb43);
        // 加入到JFrame
        setLayout(new GridLayout(4,1));
        this.add(jp1);
        this.add(jp2);
        this.add(jp3);
        this.add(jp4);
    }
    //构造器
    /**
     * MonthPanel布局
     * */
    public MonthPanel() {
        createPanel();
    }
    //方法
    /**
     * 检查输入信息格式是否正确
     * */
    public boolean check_month_str(MonthPanel month){
        if(month.jrb5.isSelected()){
            //选择不指定按钮
            return true;
//            month_str="?";
        }
        else if(month.jrb2.isSelected()){
            //范围
            if(month.jtx31.getText().equals("") || month.jtx32.getText().equals("")){
                return false;
            }
            int tx1=Integer.getInteger(month.jtx31.getText()),tx2=Integer.getInteger(month.jtx32.getText());
            if(tx1>=1 && tx1<12 && tx2>1 &&tx2<=12 && tx1<tx2){
                return true;
            }else{
                return false;
            }
        }
        else if(month.jrb3.isSelected()){
//            System.out.println("间隔");
//            int
//            month_str=month.jtx41.getText()+"\\"+month.jtx42.getText();
            //这部分需要后面再看
            return true;
//            System.out.println(month_str);
        }
        else if (month.jrb4.isSelected()){
//            System.out.println("指定");
            for(int i=0;i<12;++i){
                if(month.jcbs[i].isSelected()){
                    return true;
                }
            }
            return false;
        }
        else{
            return true;
        }
    }
    /**
     * 获取对应输入信息的Month部分的cron表达式
     * */
    public String get_month_str(MonthPanel month){
        String month_str="";
        if(month.jrb5.isSelected()){
            //如果单选框中，第一个单选项被选中了
            //all
//            System.out.println("*");
            month_str="?";
        }
        else if(month.jrb2.isSelected()){
            //范围
//            System.out.println("范围");
            month_str=month.jtx31.getText()+"-"+month.jtx32.getText();
//            System.out.println(month_str);
        }
        else if(month.jrb3.isSelected()){
//            System.out.println("间隔");
            month_str=month.jtx41.getText()+"\\"+month.jtx42.getText();
//            System.out.println(month_str);
        }
        else if (month.jrb4.isSelected()){
//            System.out.println("指定");
            int is_first=0;
            for(int i=0;i<12;++i){
                if(month.jcbs[i].isSelected()){
                    if(is_first<=0){
                        is_first+=1;
                        month_str+=month.jcbs[i].getText();
                    }else{
                        month_str+=","+month.jcbs[i].getText();
                    }
                }
            }
        }
        else{
            month_str="*";
        }
        return month_str;
    }
}
