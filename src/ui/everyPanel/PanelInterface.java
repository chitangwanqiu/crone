package ui.everyPanel;

/**
 * @author 李朔
 * @version 1.0
 * 创建一个panel接口
 */
public interface PanelInterface {

    public void createPanel();
}
