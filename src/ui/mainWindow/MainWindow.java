package ui.mainWindow;


import cronCreationAndParse.cronCreation.CronCreationAddYear;
import cronCreationAndParse.cronParse.parseDescStr.parseDesc.ParseStrDescAddYear;
import cronCreationAndParse.cronParse.parseNextTime.AvoidHolidayCronParseDecorator;
import cronCreationAndParse.cronParse.parseNextTime.CronParseDecorator;
import cronCreationAndParse.cronParse.parseNextTime.CronSequenceGenerator;
import ui.everyPanel.*;
import ui.facedePattern.FacadePatternPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//主界面窗口类
public class MainWindow extends JFrame{
    //属性
    private String page_name[]={"秒","分","时","日","月","周","年"};
    private JTabbedPane p;
    private JPanel jp1,jp2,jp3;
    private JButton jb1,jb2;//按钮
    private  JTextField jtx1,jtx2;//文本框
    private JTextArea jta;
    //构造器
    /**
     * 主界面布局搭建和按钮事件定义
     * */
    public MainWindow(){
        //设置窗口大小以及是否可见
        setBounds(10,10,1000,800);
        setVisible(true);
        //对象实例化
        p=new JTabbedPane(JTabbedPane.LEFT);
        jp1=new JPanel();
        jp2=new JPanel();
        jp3=new JPanel();
        jb1=new JButton("反解析cron");
        jb2=new JButton("生成cron");
        jta=new JTextArea(5,40);
        jtx1=new JTextField(15);
        jtx2=new JTextField(15);
        //组件尺寸调整
        jb1.setPreferredSize(new Dimension(160,50));
        jb2.setPreferredSize(new Dimension(160,50));
        jb1.setFont(new Font("宋体",Font.BOLD, 16));
        jb2.setFont(new Font("宋体",Font.BOLD, 16));
        jtx1.setPreferredSize(new Dimension(800,50));
        jtx2.setPreferredSize(new Dimension(800,50));
//        jta.setFont(new Font("宋体",Font.BOLD, 16));
        jtx1.setFont(new Font("宋体",Font.BOLD, 16));
        jtx2.setFont(new Font("宋体",Font.BOLD, 16));
        jp1.add(jtx1);
        jp1.add(jb1);
        jp1.add(jtx2);
        jp1.add(jb2);
        jp2.add(jta);
        jp3.setLayout(new GridLayout(2,1,0,0));
        jp3.add(jp1);
        jp3.add(jp2);
        //各个时间模块场景的搭建工作
       ArrayList<JPanel> jPanels= FacadePatternPanel.createPanels(page_name[0],page_name[1],p,page_name);
       SecAndMinutePanel secAndMinute=(SecAndMinutePanel) jPanels.get(0);
       SecAndMinutePanel secAndMinute1=(SecAndMinutePanel) jPanels.get(1);
       HourPanel hour=(HourPanel) jPanels.get(2);
       DayPanel day=(DayPanel) jPanels.get(3);
       MonthPanel month=(MonthPanel) jPanels.get(4);
       WeekPanel week=(WeekPanel) jPanels.get(5);
       YearPanel year=(YearPanel) jPanels.get(6);
        p.add(page_name[0],secAndMinute);
        p.add(page_name[1],secAndMinute1);
        p.add(page_name[2],hour);
        p.add(page_name[3],day);
        p.add(page_name[4],month);
        p.add(page_name[5],week);
        p.add(page_name[6],year);
        p.validate();

        add(p,BorderLayout.CENTER);
        add(jp3,BorderLayout.SOUTH);

        jp3.setPreferredSize(new Dimension(800,200));

        validate();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //按钮单击事件重写
        //cron表达式的生成
        /**
         * cron表达式生成
         * */
        jb2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!secAndMinute.check_sec_minute_info(secAndMinute)) {
                    //如果秒的部分不符合规则
                    JOptionPane.showMessageDialog(null, "秒部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                } else if (!secAndMinute1.check_sec_minute_info(secAndMinute1)) {
                    JOptionPane.showMessageDialog(null, "分部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                } else if (!hour.check_hour_str(hour)) {
                    JOptionPane.showMessageDialog(null, "时部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                } else if (!day.check_day_str(day)) {
                    JOptionPane.showMessageDialog(null, "天部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                } else if (!month.check_month_str(month)) {
                    JOptionPane.showMessageDialog(null, "月部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                } else if (!week.check_week_str(week)) {
                    JOptionPane.showMessageDialog(null, "周部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                }
                else if(!new CronCreationAddYear().checkWeekDay(week.getJrb5SelectedInfo(),day.getJrb5SelectedInfo())){
                    JOptionPane.showMessageDialog(null, "周部分信息与日期部分冲突，请重新输入...", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                }
                else if (!year.check_year_str(year)) {
                    JOptionPane.showMessageDialog(null, "年部分输入信息有误，请重新输入..", "信息有误",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    System.out.println("正在生成cron表达式....");
                    String sec_str = secAndMinute.get_sec_minute_str(secAndMinute);
                    String minute_str = secAndMinute1.get_sec_minute_str(secAndMinute1);
                    String hour_str = hour.get_hour_str(hour);
                    String day_str = day.get_day_str(day);
                    String month_str = month.get_month_str(month);
                    String week_str = week.get_week_str(week);
                    String year_str = year.get_year_str(year);
                    jtx2.setText(new CronCreationAddYear().cronCreation(sec_str, minute_str, hour_str, day_str, month_str, week_str, year_str));
                }
            }
        });
        //cron表达式解析
        /**
         * cron表达式解析，并返回下一个符合要求的时间
         * */
        jb1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("正在解析cron表达式..");
                String croneStr=jtx1.getText();
                String[] fieldStrs=croneStr.split(" ");
                //创建装饰器对象
                CronParseDecorator cronParseDecorator=new AvoidHolidayCronParseDecorator(new CronSequenceGenerator(croneStr));
                //创建描述解析对象
                ParseStrDescAddYear parseStrDescAddYear = new ParseStrDescAddYear();
                //获取解析cron表达式的描述字符串
                if(fieldStrs.length==7) {
                    String parseStr = parseStrDescAddYear.getParseStr(fieldStrs);
                    jtx1.setText(parseStr);
                }
                if(fieldStrs.length==6){
                    //暂不做
                }
                //解析下一个符合要求的时间
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date= new Date();
                Date date1=cronParseDecorator.next(date);
                String str1 = sdf.format(date1);
                jta.setText(str1);
            }
        });
    }
}