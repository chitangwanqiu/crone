package ui.facedePattern;

import ui.everyPanel.SecAndMinutePanel;

import javax.swing.*;
import java.util.ArrayList;

/**
 * @author 李朔
 * @version 1.0
 * 外观模式的实现类
 */
public class FacadePatternPanel {
    public static ArrayList<JPanel> createPanels(String s1, String s2, JTabbedPane p, String[] page_name){
        PanelMaker panelMaker = new PanelMaker();
        ArrayList<JPanel> panels=new ArrayList<>();
        SecAndMinutePanel secAndMinute=panelMaker.createSecOrMinutePanel(s1);
        panels.add(secAndMinute);
        SecAndMinutePanel secAndMinute1=panelMaker.createSecOrMinutePanel(s2);
        panels.add(secAndMinute1);
        JPanel hour=panelMaker.createHourPanel();
        panels.add(hour);
        JPanel day=panelMaker.createDayPanel();
        panels.add(day);
        JPanel month=panelMaker.createMonthPanel();
        panels.add(month);
        JPanel week=panelMaker.createWeekPanel();
        panels.add(week);
        JPanel year=panelMaker.createYearPanel();
        panels.add(year);
        return panels;
    }
}
