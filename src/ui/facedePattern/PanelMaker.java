package ui.facedePattern;
import ui.everyPanel.SecAndMinutePanel;
import ui.panelFactory.*;
import javax.swing.*;
/**
 * @author 李朔
 * @version 1.0
 * 用来创建各个模块,外观模式的具体实现各个模块创建的类
 */
public class PanelMaker {

    //生成HourPanel
    public JPanel createHourPanel(){
        PanelFactory panelFactory=new HourPanelFactory();
        return panelFactory.factory();
    }
    //生成DayPanel
    public JPanel createDayPanel(){
        PanelFactory panelFactory=new DayPanelFactory();
        return panelFactory.factory();
    }
    //生成MonthPanel
    public JPanel createMonthPanel(){
        PanelFactory panelFactory=new MonthPanelFactory();
        return panelFactory.factory();
    }
    //生成WeekPanel
    public JPanel createWeekPanel(){
        PanelFactory panelFactory=new WeekPanelFactory();
        return panelFactory.factory();
    }
    //生成SecPanel 和MinutePanel
    public SecAndMinutePanel createSecOrMinutePanel(String SecOrMinute){
        return new SecAndMinutePanel(SecOrMinute);
    }
    //生成YearPanel
    public JPanel createYearPanel(){
        PanelFactory panelFactory=new YearPanelFactory();
        return panelFactory.factory();
    }
}
